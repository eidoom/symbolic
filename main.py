#!/usr/bin/env python3

import functools, operator, math, argparse


def is_op(i):
    return isinstance(i, str) and i[0] == "o"


def show_graph(g, b=False):
    if not isinstance(g, list):
        return str(g)

    out = ("(" if b else "") if len(g[0]) == 1 else f"{g[0]}("
    for i, e in enumerate(g[1:]):
        if i != 0 and len(g[0]) == 1:
            out += g[0]
        out += show_graph(e, len(g[0]) == 1)
    return out + (")" if b else "")


def node_count(g):
    return 1 + sum(node_count(arg) if isinstance(arg, list) else 1 for arg in g[1:])


def leaf_count(g):
    return sum(leaf_count(arg) if isinstance(arg, list) else 1 for arg in g[1:])


def symbol_count(g):
    return sum(
        symbol_count(arg) if isinstance(arg, list) else isinstance(arg, str)
        for arg in g[1:]
    )


def op_count(g):
    return 1 + sum(op_count(arg) for arg in g[1:] if isinstance(arg, list))


def replacement(g, r):
    # mutates g
    for i, arg in enumerate(g[1:], start=1):
        if isinstance(arg, list):
            replacement(arg, r)
        elif arg in r:
            g[i] = r[arg]


def check_single_args(op, args):
    if len(args) > 1:
        raise Exception(
            f"Error in ({op} {' '.join(map(str,args))}). "
            f"Operation {op} only supports 1 argument. "
            f"{len(args)} were given."
        )


def check_double_args(op, args):
    if len(args) > 2:
        raise Exception(
            f"Error in ({op} {' '.join(map(str,args))}). "
            f"Operation {op} only supports 2 arguments. "
            f"{len(args)} were given."
        )


def op(op, args):
    match op:
        case "+":
            return sum(args)
        case "-":
            return functools.reduce(operator.sub, args)
        case "*":
            return functools.reduce(operator.mul, args)
        case "/":
            gcd = math.gcd(*args)
            return 1 if gcd == max(args) else ["/", *(a // gcd for a in args)]
        case "^":
            check_double_args(op, args)
            return args[0] ** args[1]
        case "%":
            check_double_args(op, args)
            return args[0] ** args[1]
        case "abs":
            check_single_args(op, args)
            return abs(args[0])
        case _:
            raise Exception(f"Operation {op} not supported")


def update_once(g):
    # mutates g
    if all(isinstance(a, int) for a in g[1:]):
        return op(g[0], g[1:])
    else:
        for i, a in enumerate(g[1:], start=1):
            if isinstance(a, list) and (u := update_once(a)):
                g[i] = u


def update(g):
    while True:
        c = node_count(g)
        update_once(g)
        if node_count(g) == c:
            break


def eval(g):
    update(g)
    if not any(isinstance(a, list) for a in g[1:]):
        return op(g[0], g[1:])


def test_eval():
    # 3(|a-1|/4+b^2)

    g = ["*", 3, ["+", ["/", ["abs", ["-", "a", 1]], 4], ["^", "b", 2]]]

    # print(show_graph(g))
    # print(leaf_count(g))
    # print(symbol_count(g))
    # print(op_count(g))

    r = {
        "a": 5,
        "b": 6,
    }

    replacement(g, r)

    e = eval(g)

    assert e == 111


# MOPS = ("*", "+", "^", "/", "-", "%")
POPS = ("abs",)


def parse(ss):
    if ss[:3] in POPS:
        return [ss[:3], parse(ss[3:])]

    c = 0
    for i, s in enumerate(ss):
        match s:
            case "(":
                c += 1
            case ")":
                if i == len(ss) - 1 and c == 1:
                    return parse(ss[1:-1])
                else:
                    c -= 1
            case "*" | "+" | "^" | "/" | "-":
                if c == 0:
                    return [s, parse(ss[:i]), parse(ss[i + 1 :])]

    return int(ss) if ss.isnumeric() else ss


def test_parse():
    # would like it to work without the extra brackets around abs(a-1)
    s = "(((abs(a-1))/4)+(b^2))*3"
    g = ["*", ["+", ["/", ["abs", ["-", "a", 1]], 4], ["^", "b", 2]], 3]
    assert parse(s) == g
    # assert show_graph(parse(s)) == s

    # works as I want
    g = ["*", ["+", ["/", ["abs", ["-", "a", 1]], 4], ["^", "b", 2]], 3]
    assert show_graph(g) == "((abs(a-1)/4)+(b^2))*3"
    # assert parse(show_graph(g)) == g


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", type=str)
    parser.add_argument("-t", "--test", action="store_true")
    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    if args.test:
        test_eval()
        test_parse()

    if args.input:
        g = parse(args.input)
        update(g)
        e = eval(g)
        if e:
            print(e)
        else:
            print(show_graph(g))
